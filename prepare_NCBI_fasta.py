#! /usr/bin/env python

import re
import sys
import argparse
import ncbi.datasets    # install with `pip install ncbi-datasets-pylib`
from Bio import SeqIO
from argparse import RawTextHelpFormatter


def format_fasta(sequence, length_line):
    output_sequence = ''
    length = len(sequence)
    nb_iterations = length // length_line
    for i in range(0, nb_iterations):
        output_sequence += sequence[ i*length_line:(i+1)*length_line ] + "\n";
    if nb_iterations*length_line < length:
        output_sequence += sequence[ nb_iterations*length_line:] + "\n"
    return output_sequence


def get_taxid_from_taxname(taxname):
    """
    Returns a string
    """
    taxid = -1
    api_client = ncbi.datasets.ApiClient()
    taxonomy_instance = ncbi.datasets.TaxonomyApi(api_client)
    data = taxonomy_instance.tax_name_query(taxon_query=taxname)
    try:
        for taxon in data.sci_name_and_ids:
            if taxon.sci_name == taxname:
                taxid = taxon.tax_id
                break
    except:
        pass
    return taxid


def get_taxname_from_taxid(taxid):
    taxname = ''
    api_client = ncbi.datasets.ApiClient()
    taxonomy_instance = ncbi.datasets.TaxonomyApi(api_client)
    data = taxonomy_instance.taxonomy_metadata(taxons=[taxid])
    try:
        for taxon in data.taxonomy_nodes:
            if taxon.taxonomy.tax_id == int(taxid):
                taxname = taxon.taxonomy.organism_name
                break
    except:
        pass
    return taxname


def get_rank_from_tax_id(taxid):
    """
    Returns the 'rank' given the taxid
    'SPECIES', 'GENUS', 'FAMILY'...
    Returns a str.
    """
    rank = ''
    api_client = ncbi.datasets.ApiClient()
    taxonomy_instance = ncbi.datasets.TaxonomyApi(api_client)
    data = taxonomy_instance.taxonomy_metadata(taxons=[str(taxid)])
    try:
        for taxon in data.taxonomy_nodes:   # there should be only one taxon but let's check it
            if taxon.taxonomy.tax_id == int(taxid):
                rank = taxon.taxonomy.rank
                break
    except:
        pass
    return str(rank)


def get_parent_id_from_tax_id(taxid, rank):
    """
    Returns the id of taxid's parent at given 'rank' (example 'GENUS, 'FAMILY'...)
    Returns a str.
    """
    parent_id = -1
    level = -1
    api_client = ncbi.datasets.ApiClient()
    taxonomy_instance = ncbi.datasets.TaxonomyApi(api_client)
    data = taxonomy_instance.taxonomy_metadata(taxons=[str(taxid)])
    try:
        for taxon in data.taxonomy_nodes:   # there should be only one taxon but let's check it
            if taxon.taxonomy.tax_id == int(taxid):
                parent_id = taxon.taxonomy.lineage[level]
                parent_rank = get_rank_from_tax_id(parent_id)
                while parent_rank != rank:
                    level -= 1
                    parent_id = taxon.taxonomy.lineage[level]
                    parent_rank = get_rank_from_tax_id(parent_id)
                parent_id = taxon.taxonomy.lineage[level]
                break
    except AttributeError:
        # tax_id is not found. Returns parent_id init value which is -1
        pass
    return str(parent_id)



def main():
    
    desc = "Rename headers from a NCBI proteic fasta file for use with PepProHTP.\n"
    desc += "Input fasta file must contain orthologs from the same protein."

    command = argparse.ArgumentParser(prog = 'prepare_NCBI_fasta.py',
        description = desc,
        formatter_class=RawTextHelpFormatter,
        usage = '%(prog)s [options]')

    command.add_argument('-i', '--input', nargs = '?',
        type = str,
        required = True,
        help = 'Input fasta file (required)')

    command.add_argument('-o', '--output', nargs = '?',
        type = str,
        required = True,
        help = 'Output fasta file (required)')

    command.add_argument('-p', '--protein', nargs = '?',
        type = str,
        required = True,
        help = 'Protein name that will be written in fasta headers (required)')

    command.add_argument('-l', '--length', nargs = '?',
        type = int,
        default = 70,
        help = 'Output fasta line length. 0 for writing sequences on one line (defaut 70)')

    args = command.parse_args()

    taxname_regex = re.compile('(\S*).*\[(.*)\]')

    with open(args.output, 'w') as out_fh:
        for record in SeqIO.parse(args.input, 'fasta'):
            taxname_regex_result = taxname_regex.search(record.description)
            if taxname_regex_result:
                prot_id = taxname_regex_result.group(1)
                taxname = taxname_regex_result.group(2)
                taxid = get_taxid_from_taxname(taxname)
                genus_id = get_parent_id_from_tax_id(taxid, "GENUS")
                family_id = get_parent_id_from_tax_id(taxid, "FAMILY")
                genus_name = get_taxname_from_taxid(genus_id)
                family_name = get_taxname_from_taxid(family_id)
                new_header = f'{family_name}_{genus_name}_{taxname.replace(" ", "_")}_{args.protein}_{prot_id}'
                out_fh.write( '>' + new_header + "\n")
                out_fh.write( format_fasta(str(record.seq), args.length) + "\n")


if __name__ == '__main__':
    main()
    sys.exit(0)
