#!/usr/bin/env python3

import os
import sys
import argparse

from lib.combinations import *
from lib.unique_peptides import *


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-i","--peptidesCSVFile", required=True, help="Input CSV file created by rpg (required)", type=str)
    parser.add_argument("-o", "--resultsPath", required=True, help="The directory (path) wich will contain all the result files (required)", type=str)
    parser.add_argument("-p", "--peptideThreshold", help="The minimum length of the peptides we keep (default 1)", type=int, default=1)
    parser.add_argument("-c", "--combinationThreshold", help="The maximum size of a combination (default no limit)", type=int, default=0)
    args = parser.parse_args()

    if not os.path.exists(args.resultsPath):
        os.makedirs(args.resultsPath)
    else:
        if os.listdir(args.resultsPath):
            raise Exception("This directory already exists. Please give another directory.")

    # Create a log file
    log_file = os.path.join(args.resultsPath, 'log.txt')
    log_fh = open(log_file, 'w')

    # Create a list of Peptide objects
    pep = parse_csv(args.peptidesCSVFile, args.peptideThreshold)

    # Create a dict which contains, for each peptide, an array with all peptides with the same sequence
    dict_p = compare_peptide(pep)

    # Write all peptides in 'peptide_list.txt' file
    pretty_print_all(dict_p, args.resultsPath)

    # Create an array of unique peptides (Peptide objects)
    u = unique_peptide(dict_p)  # array of unique peptides (Peptide objects)

    # Create a set with all protein names
    listeAllPeptides = [elt.get_prot_name() for elt in pep]
    listeAllPeptides = set(listeAllPeptides)

    # Print unique peptides in CSV and fasta files
    pretty_print_unique_peptides_csv(u, listeAllPeptides, args.resultsPath, log_fh)
    pretty_print_unique_peptides_fasta(u, args.resultsPath)

    # For sequences without unique peptides, search for a combination (for the sequence, the genus or the family)
    mainSearchCombinations(dict_p, u, args.resultsPath, log_fh, args.combinationThreshold)

    log_fh.close()

